From f15dfd86c4fba78881071dd0f5c63466fa9737a2 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?S=C3=B6ren=20Tempel?= <soeren+git@soeren-tempel.net>
Date: Tue, 8 Feb 2022 20:29:30 +0100
Subject: [PATCH 01/11] ed: don't use memcpy with overlapping memory regions
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

The memcpy invocations in the subCommand function, modified by this
commit, previously used memcpy with overlapping memory regions. This is
undefined behavior. On Alpine Linux, it causes BusyBox ed to crash since
we compile BusyBox with -D_FORTIFY_SOURCE=2 and our fortify-headers
implementation catches this source of undefined behavior [0]. The issue
can only be triggered if the replacement string is the same size or
shorter than the old string.

Looking at the code, it seems to me that a memmove(3) is what was
actually intended here, this commit modifies the code accordingly.

[0]: https://gitlab.alpinelinux.org/alpine/aports/-/issues/13504

Signed-off-by: Sören Tempel <soeren+git@soeren-tempel.net>
Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 editors/ed.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/editors/ed.c b/editors/ed.c
index 209ce9942..4a84f7433 100644
--- a/editors/ed.c
+++ b/editors/ed.c
@@ -720,7 +720,7 @@ static void subCommand(const char *cmd, int num1, int num2)
 		if (deltaLen <= 0) {
 			memcpy(&lp->data[offset], newStr, newLen);
 			if (deltaLen) {
-				memcpy(&lp->data[offset + newLen],
+				memmove(&lp->data[offset + newLen],
 					&lp->data[offset + oldLen],
 					lp->len - offset - oldLen);
 
-- 
2.40.1


From c0bb90e2fe56b74af34047d446e7758172bbf4b7 Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Wed, 22 Feb 2023 10:50:14 +0100
Subject: [PATCH 02/11] unzip: clear SUID/GID bits, implement -K to not clear
 them

function                                             old     new   delta
unzip_main                                          2656    2715     +59
packed_usage                                       34517   34552     +35
.rodata                                           105250  105251      +1
------------------------------------------------------------------------------
(add/remove: 0/0 grow/shrink: 3/0 up/down: 95/0)               Total: 95 bytes

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 archival/unzip.c | 15 ++++++++++++---
 1 file changed, 12 insertions(+), 3 deletions(-)

diff --git a/archival/unzip.c b/archival/unzip.c
index b27dd2187..691a2d81b 100644
--- a/archival/unzip.c
+++ b/archival/unzip.c
@@ -56,7 +56,7 @@
 //kbuild:lib-$(CONFIG_UNZIP) += unzip.o
 
 //usage:#define unzip_trivial_usage
-//usage:       "[-lnojpq] FILE[.zip] [FILE]... [-x FILE]... [-d DIR]"
+//usage:       "[-lnojpqK] FILE[.zip] [FILE]... [-x FILE]... [-d DIR]"
 //usage:#define unzip_full_usage "\n\n"
 //usage:       "Extract FILEs from ZIP archive\n"
 //usage:     "\n	-l	List contents (with -q for short form)"
@@ -66,6 +66,7 @@
 //usage:     "\n	-p	Write to stdout"
 //usage:     "\n	-t	Test"
 //usage:     "\n	-q	Quiet"
+//usage:     "\n	-K	Do not clear SUID bit"
 //usage:     "\n	-x FILE	Exclude FILEs"
 //usage:     "\n	-d DIR	Extract into DIR"
 
@@ -494,6 +495,7 @@ int unzip_main(int argc, char **argv)
 		OPT_l = (1 << 0),
 		OPT_x = (1 << 1),
 		OPT_j = (1 << 2),
+		OPT_K = (1 << 3),
 	};
 	unsigned opts;
 	smallint quiet = 0;
@@ -559,7 +561,7 @@ int unzip_main(int argc, char **argv)
 
 	opts = 0;
 	/* '-' makes getopt return 1 for non-options */
-	while ((i = getopt(argc, argv, "-d:lnotpqxjv")) != -1) {
+	while ((i = getopt(argc, argv, "-d:lnotpqxjvK")) != -1) {
 		switch (i) {
 		case 'd':  /* Extract to base directory */
 			base_dir = optarg;
@@ -602,6 +604,10 @@ int unzip_main(int argc, char **argv)
 			opts |= OPT_j;
 			break;
 
+		case 'K':
+			opts |= OPT_K;
+			break;
+
 		case 1:
 			if (!src_fn) {
 				/* The zip file */
@@ -819,7 +825,10 @@ int unzip_main(int argc, char **argv)
 # endif
 			if ((cdf.fmt.version_made_by >> 8) == 3) {
 				/* This archive is created on Unix */
-				dir_mode = file_mode = (cdf.fmt.external_attributes >> 16);
+				file_mode = (cdf.fmt.external_attributes >> 16);
+				if (!(opts & OPT_K))
+					file_mode &= ~(mode_t)(S_ISUID | S_ISGID);
+				dir_mode = file_mode;
 			}
 		}
 #endif
-- 
2.40.1


From 8d198665a0d09c3f22f3a12234809f46b1c2b04a Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Wed, 29 Mar 2023 15:17:00 +0200
Subject: [PATCH 03/11] libbb/sha: fix sha-NI instruction detection

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 libbb/hash_md5_sha.c | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/libbb/hash_md5_sha.c b/libbb/hash_md5_sha.c
index 880ffab01..bbe58c77b 100644
--- a/libbb/hash_md5_sha.c
+++ b/libbb/hash_md5_sha.c
@@ -1178,7 +1178,7 @@ void FAST_FUNC sha1_begin(sha1_ctx_t *ctx)
 		if (!shaNI) {
 			unsigned eax = 7, ebx = ebx, ecx = 0, edx = edx;
 			cpuid(&eax, &ebx, &ecx, &edx);
-			shaNI = ((ebx >> 29) << 1) - 1;
+			shaNI = ((ebx >> 28) & 2) - 1; /* bit 29 -> 1 or -1 */
 		}
 		if (shaNI > 0)
 			ctx->process_block = sha1_process_block64_shaNI;
@@ -1232,7 +1232,7 @@ void FAST_FUNC sha256_begin(sha256_ctx_t *ctx)
 		if (!shaNI) {
 			unsigned eax = 7, ebx = ebx, ecx = 0, edx = edx;
 			cpuid(&eax, &ebx, &ecx, &edx);
-			shaNI = ((ebx >> 29) << 1) - 1;
+			shaNI = ((ebx >> 28) & 2) - 1; /* bit 29 -> 1 or -1 */
 		}
 		if (shaNI > 0)
 			ctx->process_block = sha256_process_block64_shaNI;
-- 
2.40.1


From 64e967fabb1cf6c190b76f51c69e0ecb5fa49be5 Mon Sep 17 00:00:00 2001
From: Ron Yorston <rmy@pobox.com>
Date: Fri, 24 Mar 2023 15:16:35 +0000
Subject: [PATCH 04/11] lineedit: fix matching of directories when searching
 PATH

Commit 8baa643a3 (lineedit: match local directories when searching
PATH) included subdirectories of the current directory in the search
when tab-completing commands.

Unfortunately a short time later commit 1d180cd74 (lineedit: use
strncmp instead of is_prefixed_with (we know the length)) broke
this feature by returning an incorrect length for the array of paths.

Fix the length and reinstate matching of subdirectories.

Signed-off-by: Ron Yorston <rmy@pobox.com>
Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 libbb/lineedit.c | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/libbb/lineedit.c b/libbb/lineedit.c
index d6b2e76ff..ed7c42ba8 100644
--- a/libbb/lineedit.c
+++ b/libbb/lineedit.c
@@ -825,8 +825,8 @@ static unsigned path_parse(char ***p)
 		res[npth++] = tmp;
 	}
 	/* special case: "match subdirectories of the current directory" */
-	/*res[npth++] = NULL; - filled by xzalloc() */
-	return npth;
+	/*res[npth] = NULL; - filled by xzalloc() */
+	return npth + 1;
 }
 
 /* Complete command, directory or file name.
-- 
2.40.1


From 07bc5de67b2724a6ed9bb980f3439504b67e20ec Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Mon, 3 Apr 2023 14:01:36 +0200
Subject: [PATCH 05/11] ash: fix broken new mail detection

Mea culpa, in "Do not allocate stack string in padvance" commit
(I left an extraneous "break" statement).

function                                             old     new   delta
cmdloop                                              329     398     +69

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 shell/ash.c | 1 -
 1 file changed, 1 deletion(-)

diff --git a/shell/ash.c b/shell/ash.c
index 18ccc1329..c15bce7ae 100644
--- a/shell/ash.c
+++ b/shell/ash.c
@@ -11287,7 +11287,6 @@ chkmail(void)
 		if (!len)
 			break;
 		p = stackblock();
-			break;
 		if (*p == '\0')
 			continue;
 		for (q = p; *q; q++)
-- 
2.40.1


From 90f5f2a190bca489fac513a150ffab79c6f585b2 Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Mon, 3 Apr 2023 14:31:18 +0200
Subject: [PATCH 06/11] ash: fix still-broken new mail detection

padvance() exit condition is return value < 0, not == 0.
After MAIL changing twice, the logic erroneously
concluded that "you have new mail".

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 shell/ash.c | 8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)

diff --git a/shell/ash.c b/shell/ash.c
index c15bce7ae..9344e4de1 100644
--- a/shell/ash.c
+++ b/shell/ash.c
@@ -11264,8 +11264,8 @@ static smallint mail_var_path_changed;
 /*
  * Print appropriate message(s) if mail has arrived.
  * If mail_var_path_changed is set,
- * then the value of MAIL has mail_var_path_changed,
- * so we just update the values.
+ * then the value of MAIL has changed,
+ * so we just update the hash value.
  */
 static void
 chkmail(void)
@@ -11284,7 +11284,7 @@ chkmail(void)
 		int len;
 
 		len = padvance_magic(&mpath, nullstr, 2);
-		if (!len)
+		if (len < 0)
 			break;
 		p = stackblock();
 		if (*p == '\0')
@@ -11305,8 +11305,8 @@ chkmail(void)
 	if (!mail_var_path_changed && mailtime_hash != new_hash) {
 		if (mailtime_hash != 0)
 			out2str("you have mail\n");
-		mailtime_hash = new_hash;
 	}
+	mailtime_hash = new_hash;
 	mail_var_path_changed = 0;
 	popstackmark(&smark);
 }
-- 
2.40.1


From 515adcc9f3eb437400c08c24e0f19b149aed7f06 Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Mon, 3 Apr 2023 19:29:57 +0200
Subject: [PATCH 07/11] hush: printf builtin with no arguments should not exit

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 coreutils/printf.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/coreutils/printf.c b/coreutils/printf.c
index 2e672d15f..b89df67f9 100644
--- a/coreutils/printf.c
+++ b/coreutils/printf.c
@@ -428,7 +428,7 @@ int printf_main(int argc UNUSED_PARAM, char **argv)
 	if (argv[1] && argv[1][0] == '-' && argv[1][1] == '-' && !argv[1][2])
 		argv++;
 	if (!argv[1]) {
-		if (ENABLE_ASH_PRINTF
+		if ((ENABLE_ASH_PRINTF || ENABLE_HUSH_PRINTF)
 		 && applet_name[0] != 'p'
 		) {
 			bb_simple_error_msg("usage: printf FORMAT [ARGUMENT...]");
-- 
2.40.1


From d661cb1977def8215c50ae3eed1f9beb2877b862 Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Mon, 3 Apr 2023 19:54:42 +0200
Subject: [PATCH 08/11] ash: sleep builtin with no arguments should not exit

function                                             old     new   delta
sleep_main                                           116     143     +27
.rodata                                           105245  105268     +23
------------------------------------------------------------------------------
(add/remove: 0/0 grow/shrink: 2/0 up/down: 50/0)               Total: 50 bytes

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 coreutils/sleep.c | 15 ++++++++++++++-
 procps/kill.c     |  2 +-
 2 files changed, 15 insertions(+), 2 deletions(-)

diff --git a/coreutils/sleep.c b/coreutils/sleep.c
index 442841210..667db558d 100644
--- a/coreutils/sleep.c
+++ b/coreutils/sleep.c
@@ -65,15 +65,28 @@ int sleep_main(int argc UNUSED_PARAM, char **argv)
 {
 	duration_t duration;
 
+	/* Note: sleep_main may be directly called from ash as a builtin.
+	 * This brings some complications:
+	 * + we can't use xfunc here
+	 * + we can't use bb_show_usage
+	 * + applet_name can be the name of the shell
+	 */
 	++argv;
-	if (!*argv)
+	if (!*argv) {
+		/* Without this, bare "sleep" in ash shows _ash_ --help */
+		if (ENABLE_ASH_SLEEP && applet_name[0] != 's') {
+			bb_simple_error_msg("sleep: missing operand");
+			return EXIT_FAILURE;
+		}
 		bb_show_usage();
+	}
 
 	/* GNU sleep accepts "inf", "INF", "infinity" and "INFINITY" */
 	if (strncasecmp(argv[0], "inf", 3) == 0)
 		for (;;)
 			sleep(INT_MAX);
 
+//FIXME: in ash, "sleep 123qwerty" as a builtin aborts the shell
 #if ENABLE_FEATURE_FANCY_SLEEP
 	duration = 0;
 	do {
diff --git a/procps/kill.c b/procps/kill.c
index 8f10e21ab..208efebde 100644
--- a/procps/kill.c
+++ b/procps/kill.c
@@ -85,8 +85,8 @@
  * This brings some complications:
  *
  * + we can't use xfunc here
- * + we can't use applet_name
  * + we can't use bb_show_usage
+ * + applet_name can be the name of the shell
  * (doesn't apply for killall[5], still should be careful b/c NOFORK)
  *
  * kill %n gets translated into kill ' -<process group>' by shell (note space!)
-- 
2.40.1


From ce839dea92ce10627094096835e831bf5d267631 Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Mon, 10 Apr 2023 16:30:27 +0200
Subject: [PATCH 09/11] ash: fix sleep built-in not running INT trap
 immediately on ^C

function                                             old     new   delta
sleep_for_duration                                   169     149     -20

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 libbb/duration.c | 16 +++++++---------
 1 file changed, 7 insertions(+), 9 deletions(-)

diff --git a/libbb/duration.c b/libbb/duration.c
index 793d02f42..0024f1a66 100644
--- a/libbb/duration.c
+++ b/libbb/duration.c
@@ -76,16 +76,14 @@ void FAST_FUNC sleep_for_duration(duration_t duration)
 		ts.tv_sec = duration;
 		ts.tv_nsec = (duration - ts.tv_sec) * 1000000000;
 	}
-	/* NB: if ENABLE_ASH_SLEEP, we end up here if "sleep N"
-	 * is run in ash. ^C will still work, because ash's signal handler
-	 * does not return (it longjumps), the below loop
-	 * will not continue looping.
-	 * (This wouldn't work in hush)
+	/* NB: ENABLE_ASH_SLEEP requires that we do NOT loop on EINTR here:
+	 * otherwise, traps won't execute until we finish looping.
 	 */
-	do {
-		errno = 0;
-		nanosleep(&ts, &ts);
-	} while (errno == EINTR);
+	//do {
+	//	errno = 0;
+	//	nanosleep(&ts, &ts);
+	//} while (errno == EINTR);
+	nanosleep(&ts, &ts);
 }
 #else
 duration_t FAST_FUNC parse_duration_str(char *str)
-- 
2.40.1


From 384cd2e436e9b15a7ffe2bf93260d94992dc1193 Mon Sep 17 00:00:00 2001
From: Denys Vlasenko <vda.linux@googlemail.com>
Date: Wed, 12 Apr 2023 12:23:36 +0200
Subject: [PATCH 10/11] sleep: fix error exit when called as "sh" builtin

Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 coreutils/sleep.c | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

diff --git a/coreutils/sleep.c b/coreutils/sleep.c
index 667db558d..a0cee5a4a 100644
--- a/coreutils/sleep.c
+++ b/coreutils/sleep.c
@@ -74,7 +74,8 @@ int sleep_main(int argc UNUSED_PARAM, char **argv)
 	++argv;
 	if (!*argv) {
 		/* Without this, bare "sleep" in ash shows _ash_ --help */
-		if (ENABLE_ASH_SLEEP && applet_name[0] != 's') {
+		/* (ash can be the "sh" applet as well, so check 2nd char) */
+		if (ENABLE_ASH_SLEEP && applet_name[1] != 'l') {
 			bb_simple_error_msg("sleep: missing operand");
 			return EXIT_FAILURE;
 		}
-- 
2.40.1


From 38d2d26a5fec3dfd24493c8b42a90e4c413f80d5 Mon Sep 17 00:00:00 2001
From: Akos Somfai <akos.somfai@gmail.com>
Date: Mon, 3 Apr 2023 22:52:06 +0200
Subject: [PATCH 11/11] lineedit: fix crash when icanon set with -echo

When icanon is set with -echo (e.g. ssh from an emacs shell) then
S.state will remain null but later it will be deferenced causing ash to
crash. Fix: additional check on state.

Signed-off-by: Akos Somfai <akos.somfai@gmail.com>
Signed-off-by: Denys Vlasenko <vda.linux@googlemail.com>
---
 libbb/lineedit.c | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/libbb/lineedit.c b/libbb/lineedit.c
index ed7c42ba8..c87d6064a 100644
--- a/libbb/lineedit.c
+++ b/libbb/lineedit.c
@@ -254,7 +254,7 @@ static NOINLINE const char *get_homedir_or_NULL(void)
 	const char *home;
 
 # if ENABLE_SHELL_ASH || ENABLE_SHELL_HUSH
-	home = state->sh_get_var ? state->sh_get_var("HOME") : getenv("HOME");
+	home = state && state->sh_get_var ? state->sh_get_var("HOME") : getenv("HOME");
 # else
 	home = getenv("HOME");
 # endif
@@ -2038,7 +2038,7 @@ static void parse_and_put_prompt(const char *prmt_ptr)
 					if (!cwd_buf) {
 						const char *home;
 # if EDITING_HAS_sh_get_var
-						cwd_buf = state->sh_get_var
+						cwd_buf = state && state->sh_get_var
 							? xstrdup(state->sh_get_var("PWD"))
 							: xrealloc_getcwd_or_warn(NULL);
 # else
-- 
2.40.1

