# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=asteroid-compass
pkgver=0_git20230211
pkgrel=0
_commit="23d104e70c9991f7b383a6c021b65c2f19a99d39"
pkgdesc="Default compass app for AsteroidOS"
url="https://github.com/AsteroidOS/asteroid-compass"
# armhf blocked by qml-asteroid
arch="all !armhf"
license="GPL-3.0-or-later"
depends="
	mapplauncherd
	qt5-qtsensors
	"
makedepends="
	extra-cmake-modules
	qml-asteroid-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://github.com/AsteroidOS/asteroid-compass/archive/$_commit/asteroid-compass-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
48c5a3634a2afcb0ce5d8d57419d737e740c9accc139cf8793c3e38f8ca335fa5e27b4156587ce12ef6e60a2474da7ed10306ec2676617e49b19a86319b16a30  asteroid-compass-23d104e70c9991f7b383a6c021b65c2f19a99d39.tar.gz
"
