# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=py3-regex
pkgver=2023.5.5
pkgrel=0
pkgdesc="More featureful implementation of the 're' module"
url="https://github.com/mrabarnett/mrab-regex"
arch="all"
license="Apache-2.0"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/mrabarnett/mrab-regex/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/mrab-regex-$pkgver"
options="!check" # fails to run

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	cd testenv
	bin/python3 ../regex_3/test_regex.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl

	rm -v "$pkgdir"/usr/lib/python3*/site-packages/regex/test_regex.py
}

sha512sums="
3d54b1310e41e309a0b4645d38d755eb4f6c1890a88134e5c12d35054792db66e9f5e6c2d4b1efdecf03dd135ba2493cf34c7da5576f8078839224f04b158807  py3-regex-2023.5.5.tar.gz
"
