# Maintainer: Galen Abell <galen@galenabell.com>
# Contributor: Galen Abell <galen@galenabell.com>
pkgname=packer
pkgver=1.8.7
pkgrel=0
pkgdesc="tool for creating machine images for multiple platforms"
url="https://www.packer.io/"
license="MPL-2.0"
arch="all"
makedepends="go"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/packer/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -trimpath"

build() {
	export GOLDFLAGS="-X github.com/hashicorp/packer/version.Version=$pkgver -X github.com/hashicorp/packer/version.VersionPrerelease="
	go build -v -o bin/$pkgname -ldflags="$GOLDFLAGS"
}

check() {
	go list . | xargs -t -n4 go test -timeout=2m -parallel=4
	bin/$pkgname -v
}

package() {
	install -Dm755 bin/"$pkgname" -t "$pkgdir"/usr/bin/
}

sha512sums="
94f33489347f1ce90bc44943f20f40dc683680f0cc28c4a6a718f9aa1cb7228899573633b09f421bf8330b41b71dbffe17e101e75bd688db25f424b8b05470fd  packer-1.8.7.tar.gz
"
