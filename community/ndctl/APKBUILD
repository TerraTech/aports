# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Breno Leitao <breno.leitao@gmail.com>
# Maintainer:
pkgname=ndctl
pkgver=77
pkgrel=0
pkgdesc="Utility library for managing the libnvdimm (non-volatile memory device) sub-system in the Linux kernel"
url="https://github.com/pmem/ndctl"
arch="all"
license="GPL-2.0-only AND LGPL-2.1-only"
depends="kmod"
makedepends="
	asciidoc
	asciidoctor
	bash-completion
	eudev-dev
	iniparser-dev
	json-c-dev
	keyutils-dev
	kmod-dev
	libtraceevent-dev
	libtracefs-dev
	linux-headers
	meson
	util-linux-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-dev
	$pkgname-libs
	$pkgname-bash-completion
	"
source="ndctl-$pkgver.tar.gz::https://github.com/pmem/ndctl/archive/v$pkgver.tar.gz"
options="!check" # tests require building & loading the nfit_test.ko kernel module

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsystemd=disabled \
		-Dversion-tag="$pkgver" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
cd1e37d015f209df446441da162d41bbca1b740d799b383c1147d3a3b1acc4e6dc4bcc1fda0e868f305e8a1698c969426c08d8a6222d8a0c8f3c94963b0ae36e  ndctl-77.tar.gz
"
