# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=himalaya
pkgver=0.7.3
pkgrel=1
pkgdesc="CLI email client"
url="https://github.com/soywod/himalaya"
license="MIT"
arch="all !ppc64le !riscv64 !s390x" # fails to build ring crate
makedepends="cargo openssl-dev sqlite-dev"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/soywod/himalaya/archive/v$pkgver/himalaya-$pkgver.tar.gz"
options="net" # fetch dependencies

_features="--no-default-features --features native-tls,imap-backend,smtp-sender"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
	mkdir man-pages
}

build() {
	cargo build --release --frozen $_features

	./target/release/himalaya man man-pages

	./target/release/himalaya completion bash > $pkgname.bash
	./target/release/himalaya completion fish > $pkgname.fish
	./target/release/himalaya completion zsh > $pkgname.zsh
}

check() {
	# skip imap backend e2e test
	cargo test --frozen $_features -- \
		--skip test_imap_backend
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	install -Dm644 man-pages/*.1 -t "$pkgdir"/usr/share/man/man1/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
7ab7042b5dcb826e0187e67cc091f176181402c04cac3b10a23231d1d1facf9fdea2ac0d00f0e15982f8858f0ac1b36aed41190be0671b6c04f2669e3f2a42cc  himalaya-0.7.3.tar.gz
"
