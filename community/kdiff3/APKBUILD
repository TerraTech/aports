# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdiff3
pkgver=1.10.2
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/kdiff3/"
pkgdesc="A file and folder diff and merge tool"
license="GPL-2.0-or-later "
makedepends="
	boost-dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/kdiff3/kdiff3-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cbf63eae296370174c97a52213f436e326c1c1c576506d83ebca0a4c443b95aa4d69593df0e6688e13d5f187d60c81bfeb198f3681c2df8a4cfd6279bbd9cd7d  kdiff3-1.10.2.tar.xz
"
